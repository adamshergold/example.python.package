import pytest

from random import randrange

_max_int = 4096


def create_list_of_ints(n):
    return [randrange(-_max_int, _max_int) for i in range(0, randrange(0, n))]


def create_inputs(m, n):
    return [create_list_of_ints(n) for i in range(0, m)]


@pytest.fixture(params=create_inputs(100, 64))
def sorting_inputs(request):
    return request.param
