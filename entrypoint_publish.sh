#!/bin/sh -x

# exit if any command fails
set -e

poetry config --list

poetry publish -r test-pypi

