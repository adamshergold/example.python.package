FROM python:3.11.2 AS python-base
ARG APP_PATH=/app
ARG PYTHON_VERSION=3.11.2
ARG POETRY_VERSION=1.4.1

# Do not buffer stdout etc.
ENV PYTHONUNBUFFERED=1 

# avoid writing .pyc (byte-code files)
ENV PYTHONDONTWRITEBYTECODE=1 

# install a handler for SIGSEGV, SIGFPE, SIGABRT, SIGBUS and SIGILL
ENV PYTHONFAULTHANDLER=1

ENV POETRY_VERSION=$POETRY_VERSION 
ENV POETRY_HOME="/opt/poetry" 
ENV POETRY_VIRTUALENVS_IN_PROJECT=true 
ENV POETRY_NO_INTERACTION=1

# Install Poetry - respects $POETRY_VERSION & $POETRY_HOME
RUN curl -sSL https://install.python-poetry.org | python3


RUN curl -Os https://uploader.codecov.io/latest/linux/codecov
RUN mv codecov /opt
RUN chmod +x /opt/codecov

ENV PATH="$POETRY_HOME/bin:/opt:$PATH"

WORKDIR $APP_PATH
COPY poetry.lock pyproject.toml README.md ./
COPY src src
COPY tests tests


FROM python-base AS build
ARG APP_PATH
ARG BUILD_NUMBER
ARG TESTPYPI_TOKEN

WORKDIR $APP_PATH


RUN poetry config repositories.test-pypi https://test.pypi.org/legacy/

RUN if test -n "${TESTPYPI_TOKEN}"; then poetry config pypi-token.test-pypi $TESTPYPI_TOKEN; else echo "TESTPYPI_TOKEN not set"; fi

RUN poetry config --list 

RUN poetry install

RUN poetry run flake8 src

RUN if test -n "${BUILD_NUMBER}"; then poetry version "$(poetry version --short).${BUILD_NUMBER}"; else echo "Not changing poetry version"; fi

RUN poetry version 

RUN poetry build


FROM build AS test
ARG APP_PATH

WORKDIR $APP_PATH
COPY .coveragerc ./
COPY --chmod=0755 entrypoint_test.sh ./
ENTRYPOINT [ "/app/entrypoint_test.sh" ]

FROM build AS publish
WORKDIR $APP_PATH
COPY --chmod=0755 entrypoint_publish.sh ./
ENTRYPOINT [ "/app/entrypoint_publish.sh" ]
