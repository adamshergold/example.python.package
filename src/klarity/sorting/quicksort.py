def _swap(vs, x, y):
    vs[x], vs[y] = vs[y], vs[x]


def _partition(vs, start, end):
    # arbitrary / simple choice to pick pivot at end
    pivot = vs[end]
    # invariant is that items vs in [start,m] are < pivot
    m = start-1
    for i in range(start, end):
        if vs[i] < pivot:
            m += 1
            _swap(vs, m, i)
    _swap(vs, end, m+1)
    return m+1


def _impl(vs, start, end):
    if end <= start:
        return
    p = _partition(vs, start, end)
    _impl(vs, start, p-1)
    _impl(vs, p+1, end)


def sort(vs):
    _impl(vs, 0, len(vs)-1)
