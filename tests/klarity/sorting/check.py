def is_sorted(vs):
    if len(vs) < 2:
        return True
    last = vs[0]
    for v in vs[1:]:
        if v < last:
            return False
        last = v
    return True
