from klarity.sorting import quicksort as sorter
from check import is_sorted


def test_quicksort_works(sorting_inputs):
    print(f'Before = {sorting_inputs}')
    sorter.sort(sorting_inputs)
    print(f'After = {sorting_inputs}')
    assert is_sorted(sorting_inputs)
