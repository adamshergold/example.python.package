#!/bin/sh -x

# exit if any command fails
set -e

poetry run coverage run -m pytest

poetry run coverage report -m 

codecov -t ${CODECOV_TOKEN}